﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraniteHouse.Data;
using GraniteHouse.Models;
using Microsoft.AspNetCore.Mvc;

namespace GraniteHouse.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ProductTypeController : Controller
    {

        private static ApplicationDbContext _db;

        public ProductTypeController(ApplicationDbContext DB)
        {
            _db = DB;
        }

        public IActionResult Index()
        {
            return View(_db.productTypes.ToList());
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ProductTypes productTypes)
        {
            if (ModelState.IsValid)
            {
                _db.Add(productTypes);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productTypes);
        }
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ProductType = await _db.productTypes.FindAsync(id);
            if (ProductType == null)
            {
                return NotFound();
            }
            return View(ProductType);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ProductTypes productTypes)
        {
            if (id != productTypes.ID)
                return NotFound();
            if (ModelState.IsValid)
            {
                _db.Update(productTypes);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(productTypes);
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ProductType = await _db.productTypes.FindAsync(id);
            if (ProductType == null)
            {
                return NotFound();
            }
            return View(ProductType);
        }
        //delete in same view
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var ProductType = await _db.productTypes.FindAsync(id);
            if (ProductType == null)
            {
                return NotFound();
            }
            _db.productTypes.Remove(ProductType);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));

        }

        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }
        //    var ProductType = await _db.productTypes.FindAsync(id);
        //    if (ProductType == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(ProductType);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    var ProductType = await _db.productTypes.FindAsync(id);
        //    _db.productTypes.Remove(ProductType);
        //    await _db.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}
    }
}